########### Couleurs RGB ###########

# Nom de la couleur = (Densité de rouge, Densité de vert, Densité de bleu)
# Les densités vont de 0 à 255

BLANC = (255, 255, 255)
NOIR = (0, 0, 0)
ROUGE = (255, 0, 0)
VERT = (0, 255, 0)
BLEU = (0, 0, 255)
JAUNE = (255, 255, 0)
VIOLET = (255, 0, 255)
CYAN = (0, 255, 255)