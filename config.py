import os

# Variable de la boucle du jeu
marche = True

# Récupérer l'emplacement du jeu
dossier_jeu = os.path.dirname(__file__)

# Utiliser les répertoire img dans les dossiers du jeu
dossier_images = os.path.join(dossier_jeu, "img")