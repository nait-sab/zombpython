import pygame
import os
from fenetre import *
from couleur import *
from config import *

########### Joueur ###########
# Classe du joueur de type sprite

class Joueur(pygame.sprite.Sprite):


    # initiation de la classe
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)

        # Sprite utilisé pour le joueur
        # Simple cube coloré pour le moment
        self.image = pygame.Surface((50, 40))
        self.image.fill(VERT)

        # Bordure du sprite = Bordure de l'image utilisé
        # contour de la surface pour le moment
        self.rect = self.image.get_rect()

        # Centrage de la position du sprite au centre en axe x
        self.rect.centerx = LONGUEUR / 2

        # Positionnement en hauteur en bas de la fenêtre
        self.rect.bottom = HAUTEUR - 40

        # Variable de vitesse de déplacement en axe x
        self.vitesse_x = 0


    # Mise à jour en boucle du sprite
    def update(self):

        # Réinitialisation de la vitesse
        # Arrête le déplacement quand aucune touche n'est utilisée
        self.vitesse_x = 0

        # Variables gérant les événements clavier de Pygame
        touches = pygame.key.get_pressed()

        # Déplacement gauche
        if touches[pygame.K_LEFT]:
            self.vitesse_x = -8

        # Déplacement droit
        if touches[pygame.K_RIGHT]:
            self.vitesse_x = 8

        # Actualisation de la vitesse de déplacement 
        # La vitesse est modifier par les événements clavier précédent
        self.rect.x += self.vitesse_x

        # Bloquer le sprite contre les bords de la fenêtre en cas de dépassement
        if self.rect.right > LONGUEUR:
            self.rect.right = LONGUEUR
        if self.rect.left < 0:
            self.rect.left = 0