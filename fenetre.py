########### Paramètre fenêtre ###########

# Longueur et hauteur de la fenêtre du jeu
LONGUEUR = 480
HAUTEUR = 600

# Vitesse du jeu
FPS = 60

# Titre de la fenêtre (+ numéro de version de dév)
titre = "zombPython 0.2"