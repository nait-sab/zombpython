########### Shooter game
########### @Naitsab
#################################
import pygame
import random
import os

from couleur import *
from fenetre import *
from config import *
from joueur import *

########### Initialiser pygame et créer la fenêtre ###########
pygame.init()
pygame.mixer.init()
# Création de la fenêtre
fenetre = pygame.display.set_mode((LONGUEUR, HAUTEUR))
# Titre de la fenêtre
pygame.display.set_caption(titre)
# Horloge du jeu
horloge = pygame.time.Clock()

########### SPRITES ###########
# Création du joueur
joueur = Joueur()
# Création d'un goupe global de sprite
tout_les_sprites = pygame.sprite.Group()
# Insertion du sprite du joueur
tout_les_sprites.add(joueur)

########### Boucle du jeu ###########
while marche:
    # Faire le jeu à la bonne vitesse
    horloge.tick(FPS)

    # Evenements dans la fenêtre
    for evenement in pygame.event.get():
        # Evenement du bouton quitter de la fenêtre 
        if evenement.type == pygame.QUIT:
            marche = False

    # Mise à jour du groupe de sprite
    tout_les_sprites.update()

    # Colorer le fond de la fenêtre en noir
    ecran.fill(NOIR)
    # Dessiner sur la fenêtre le groupe de sprite
    tout_les_sprites.draw(fenetre)

    # Effacer la fenêtre pour actualiser
    pygame.display.flip()


# Quitter le programme
pygame.quit()